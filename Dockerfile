ARG PROXY_ADDRESS=""
#--------------------------------------------------------------------------------
# Hercule build stage
FROM ubuntu:24.04 AS build_hercule
ARG PROXY_ADDRESS

RUN if [[ -n "${PROXY_ADDRESS}" ]]; \
        then echo "Acquire::http::User-Agent \"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36\";\
          Acquire::http::proxy \"${PROXY_ADDRESS}\";\
          Acquire::https::proxy \"${PROXY_ADDRESS}\";" > /etc/apt/apt.conf.d/98user-agent-proxy;\
    else\
        echo "Not on site!";\
    fi

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends cmake ninja-build \
    qtbase5-dev qtbase5-dev-tools g++ python3-dev libglvnd-dev qttools5-dev libqt5svg5-dev \
    mpi-default-dev qtxmlpatterns5-dev-tools libqt5opengl5-dev \
    gfortran libboost-all-dev swig python3-mpi4py patch make pybind11-dev libtirpc-dev python3-setuptools git && \
    rm -rf /var/lib/apt/lists/* 

ADD Hercule/hercule-2.4.5.3_rc6_ext.tar.gz /opt

ADD Hercule/*.patch /opt/

WORKDIR /opt/hercule-master
RUN git apply ../adds_o_option_to_swig.patch

RUN git apply ../fix_gmev2_col_log_buffer_overflow.patch

RUN git apply ../fix_lm_dns_get_domaine_buffer_overflow.patch

WORKDIR /opt/hercule_build
RUN cmake -GNinja -DPYTHON_EXECUTABLE:FILEPATH=/usr/bin/python3 \
    -DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo -DCMAKE_INTERPROCEDURAL_OPTIMIZATION:BOOL=OFF \
    -DCMAKE_VERBOSE_MAKEFILE:BOOL=ON -DBUILD_SHARED_LIBS:BOOL=ON -DHERCULE_EASY_DEPS:BOOL=OFF \
    -DHERCULE_BUNDLE_MachineTypes:BOOL=ON -DHERCULE_BUNDLE_MachineIo64:BOOL=ON \
    -DHERCULE_BUNDLE_Lm:BOOL=ON -DHERCULE_BUNDLE_Gme:BOOL=ON -DHERCULE_ENABLE_OPENMP:BOOL=OFF \
    -DHERCULE_ENABLE_MPI:BOOL=ON -DHERCULE_ENABLE_FORTRAN:BOOL=OFF -DHERCULE_ENABLE_C:BOOL=OFF \
    -DHERCULE_ENABLE_PYTHON:BOOL=ON -DHERCULE_ENABLE_TOOLS:BOOL=ON \
    -DHERCULE_ENABLE_EXAMPLES:BOOL=OFF -DHERCULE_ENABLE_CCC_USER:BOOL=OFF \
    -DHERCULE_ENABLE_CCC_HSM:BOOL=OFF -DHERCULE_ENABLE_QT:BOOL=ON -DHERCULE_ENABLE_DOC:BOOL=OFF \
    -DPYBIND11_FINDPYTHON:BOOL=ON \
    -DCMAKE_INSTALL_PREFIX=/opt/hercule_install ../hercule-master

RUN cmake --build . > /opt/hercule_build.log

RUN cmake --install .

# Maybe use a .dockerignore instead?
RUN bash -c 'for fil in $(find /opt/hercule_install/* -name "*.o"); do rm $fil; done'

#--------------------------------------------------------------------------------
# Paraview build stage
FROM ubuntu:24.04 AS build_paraview
ARG PROXY_ADDRESS

RUN if [[ -n "${PROXY_ADDRESS}" ]]; \
        then echo "Acquire::http::User-Agent \"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36\";\
          Acquire::http::proxy \"${PROXY_ADDRESS}\";\
          Acquire::https::proxy \"${PROXY_ADDRESS}\";" > /etc/apt/apt.conf.d/98user-agent-proxy;\
    else\
        echo "Not on site!";\
    fi

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends git cmake ninja-build \
    autoconf automake libtool curl make g++ unzip \
    apt-transport-https ca-certificates \
    qtbase5-dev qtbase5-dev-tools g++ python3-dev libglvnd-dev qttools5-dev libqt5svg5-dev \
    mpi-default-dev qtxmlpatterns5-dev-tools libqt5opengl5-dev libpugixml-dev libdouble-conversion-dev \
    liblz4-dev liblzma-dev libjpeg-dev libpng-dev libtiff-dev libfreetype-dev libjsoncpp-dev libeigen3-dev \
    python3-mpi4py libxml2-dev libhdf5-dev libnetcdf-dev nlohmann-json3-dev libsqlite3-dev sqlite3 libproj-dev \
    libtheora-dev libprotobuf-dev catch2 protobuf-compiler && update-ca-certificates && \
    rm -rf /var/lib/apt/lists/* 

WORKDIR /opt

# COPY paraview /opt/paraview
RUN git clone https://gitlab.kitware.com/paraview/paraview.git && cd paraview && git submodule update --init --recursive

WORKDIR /opt/paraview_build

# Choose to install paraview into /opt which is part of CMAKE_SYSTEM_PREFIX_PATH variable (for Unix platform)
# This way any "find_package(ParaView)" command will automatically find paraview without the need of CMAKE_PREFIX_PATH variable
RUN cmake -GNinja \
        -DCMAKE_INSTALL_PREFIX=/opt/paraview_install \
        -DCMAKE_SKIP_INSTALL_RPATH=TRUE \
        -DCMAKE_BUILD_TYPE:STRING=RelWithDebInfo \
        -Dqt_xmlpatterns_executable=/usr/lib/qt5/bin/xmlpatterns \
        -DOpenGL_GL_PREFERENCE:STRING=LEGACY \
        -DPARAVIEW_INSTALL_DEVELOPMENT_FILES:BOOL=ON \
        -DPARAVIEW_BUILD_EDITION:STRING=CANONICAL \
        -DPARAVIEW_USE_QT:BOOL=ON \
        -DPARAVIEW_BUILD_WITH_EXTERNAL=ON \
        -DPARAVIEW_ENABLE_EXAMPLES:BOOL=OFF \
        -DPARAVIEW_QT_VERSION=5 \
        -DPARAVIEW_USE_PYTHON:BOOL=ON \
        -DPARAVIEW_USE_MPI:BOOL=ON \
        -DPARAVIEW_BUILD_SHARED_LIBS:BOOL=ON \
        -DPARAVIEW_USE_CUDA:BOOL=OFF \
        -DPARAVIEW_BUILD_WITH_KITS:BOOL=OFF \
        -DBUILD_TESTING:BOOL=OFF \
        -DVTK_OPENGL_HAS_OSMESA:BOOL=OFF \
        -DVTK_USE_X:BOOL=ON \
        -DVTK_MODULE_USE_EXTERNAL_VTK_ioss:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_exprtk:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_ogg:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_fmt:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_cgns:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_glew:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_gl2ps:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_libharu:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_utf8:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_verdict:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_token:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_fast_float:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_pegtl:BOOL=OFF \
        -DVTK_MODULE_USE_EXTERNAL_VTK_cli11:BOOL=OFF \
        ../paraview && \
        cmake --build . > /opt/paraview_build.log && \
        cmake --install .

#--------------------------------------------------------------------------------
# Final stage
FROM ubuntu:24.04 AS build_final
ARG PROXY_ADDRESS

RUN if [[ -n "${PROXY_ADDRESS}" ]]; \
        then echo "Acquire::http::User-Agent \"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/114.0.0.0 Safari/537.36\";\
          Acquire::http::proxy \"${PROXY_ADDRESS}\";\
          Acquire::https::proxy \"${PROXY_ADDRESS}\";" > /etc/apt/apt.conf.d/98user-agent-proxy;\
    else\
        echo "Not on site!";\
    fi

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends cmake ninja-build \
    qtbase5-dev qtbase5-dev-tools g++ python3-dev libglvnd-dev qttools5-dev libqt5svg5-dev \
    mpi-default-dev qtxmlpatterns5-dev-tools libqt5opengl5-dev \
    gfortran libboost-all-dev swig python3-mpi4py patch make catch2 gcovr clang-tidy-15 \
    libpugixml-dev protobuf-compiler libprotobuf-dev libxml2-dev \ 
    xorg-dev libglu1-mesa libgl1-mesa-dev \
    libhdf5-dev libtheora-dev libproj-dev \
    libnetcdf-dev libjsoncpp-dev nlohmann-json3-dev libeigen3-dev liblz4-dev libdouble-conversion-dev xvfb \
    git-all python3-numpy wget libtrompeloeil-cpp-dev cppcheck python3-pip jq python3-venv python3-sphinx \ 
    pre-commit clang-format black vim meld git-lfs iwyu libstdc++-14-dev libtirpc-dev python3-matplotlib && \
    rm -rf /var/lib/apt/lists/* 

# Special commands to install gcc8 not availabe in Ubuntu 24.04 apt store
# From https://askubuntu.com/questions/1446863/trying-to-install-gcc-8-and-g-8-on-ubuntu-22-04
RUN apt-get update && \
    wget http://mirrors.kernel.org/ubuntu/pool/universe/g/gcc-8/gcc-8_8.4.0-3ubuntu2_amd64.deb && \
    wget http://mirrors.edge.kernel.org/ubuntu/pool/universe/g/gcc-8/gcc-8-base_8.4.0-3ubuntu2_amd64.deb && \
    wget http://mirrors.kernel.org/ubuntu/pool/universe/g/gcc-8/libgcc-8-dev_8.4.0-3ubuntu2_amd64.deb && \
    wget http://mirrors.kernel.org/ubuntu/pool/universe/g/gcc-8/cpp-8_8.4.0-3ubuntu2_amd64.deb && \
    wget http://mirrors.kernel.org/ubuntu/pool/universe/g/gcc-8/libmpx2_8.4.0-3ubuntu2_amd64.deb && \
    wget http://mirrors.kernel.org/ubuntu/pool/main/i/isl/libisl22_0.22.1-1_amd64.deb && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y ./libisl22_0.22.1-1_amd64.deb \
    ./libmpx2_8.4.0-3ubuntu2_amd64.deb ./cpp-8_8.4.0-3ubuntu2_amd64.deb ./libgcc-8-dev_8.4.0-3ubuntu2_amd64.deb \
    ./gcc-8-base_8.4.0-3ubuntu2_amd64.deb ./gcc-8_8.4.0-3ubuntu2_amd64.deb && \
    wget http://mirrors.kernel.org/ubuntu/pool/universe/g/gcc-8/libstdc++-8-dev_8.4.0-3ubuntu2_amd64.deb && \
    wget http://mirrors.kernel.org/ubuntu/pool/universe/g/gcc-8/g++-8_8.4.0-3ubuntu2_amd64.deb && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y ./libstdc++-8-dev_8.4.0-3ubuntu2_amd64.deb ./g++-8_8.4.0-3ubuntu2_amd64.deb

# Install cppcheck_codequality through python3 virtual env (no apt packages and python3-11 refuses to pip install system packages)
# cppcheck_codequality is used to convert results from cppcheck into a json file interpretable by gitlab
RUN python3 -m venv code_quality_env && . code_quality_env/bin/activate && pip3 install cppcheck_codequality && deactivate

# Install clang-tidy-converter through python3 virtual env (no apt packages and python3-11 refuses to pip install system packages)
# clang-tidy-converter is used to convert clang-tidy results into a json file interpretable by gitlab
RUN git clone https://github.com/yuriisk/clang-tidy-converter.git && . code_quality_env/bin/activate && pip3 install ./clang-tidy-converter && deactivate

COPY --from=build_paraview /opt/paraview_install /opt/paraview_install
COPY --from=build_paraview /opt/paraview_build.log /opt/logs/
COPY --from=build_hercule /opt/hercule_install /opt/hercule_install
COPY --from=build_hercule /opt/hercule_build.log /opt/logs/

ENV LD_LIBRARY_PATH=/opt/paraview_install/lib

RUN rm -f /etc/apt/apt.conf.d/98user-agent-proxy
