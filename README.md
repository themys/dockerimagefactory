# DockerImageFactory

A project that builds the Docker image to be used by all [Themys](https://gitlab.com/themys) projects CI.

## Getting started

This project is a **Docker** In **Docker** (aka DIND) setup. It uses a **Docker** container that itself uses **Docker** to build the desired image.

To build locally on your machine, it is necessary to have `docker` installed.

## Installing and configuring Docker

The following step have been tested on Ubuntu 22.04.

```bash
sudo apt-get install docker-io
```

On Ubuntu 23.04 the installation has been made following the instruction found [here](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository). Do not forget to remove old installs.

If **Docker** is run behind a proxy, then it is necessary to configure the daemon, the client and the container runtime as expose [here](https://medium.com/@bennyh/docker-and-proxy-88148a3f35f7).

### Configuring Docker daemon

To configure the **Docker** daemon, edit the `/etc/systemd/system/docker.service.d/http_proxy.conf` and add:

```bash
[Service]
Environment="HTTP_PROXY=http://address:port"
Environment="HTTPS_PROXY=http://address:port"
Environment="NO_PROXY=docker:2375,docker:2376"
```

where `address:port` is the address and port of the proxy.

### Configuring the Docker client

The **Docker** client is configured through the following variable environment:

```bash
export http_proxy=http://address:port/
export https_proxy=https://address:port/
```

### Configuring the Docker container runtime

To configure the **Docker** container runtime, create or edit the file `$HOME/.docker/config.json` and add:

```json
{
    "proxies": {
        "default": {
            "httpProxy": "http://address:port",
            "httpsProxy": "http://address:port"
        }
    }
}
```

### Checking the Docker installation

It is possible to check that **Docker** is correctly installed and configured by first cloning this repository: 

```bash
git clone https://gitlab.com/themys/dockerimagefactory.git
```

and then running:

```bash
cd /path/to/dockerimagefactory
docker build --network=host --progress=plain -t test .
```

If you are behind a proxy, it could be necessary to add the proxy_address argument:

```bash
cd /path/to/dockerimagefactory
docker build --network=host --progress=plain --build-arg="PROXY_ADDRESS=<proxy_address>" -t test .
```

If everything is fine, this will output something along those lines:

```bash
Sending build context to Docker daemon   33.7MB
Step 1/26 : FROM ubuntu:21.10 AS build_paraview
 ---> 42b6dc79eba8
Step 2/26 : RUN apt-get update &&     DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends git cmake ninja-build     autoconf automake libtool curl make g++ unzip     apt-transport-https ca-certificates     qtbase5-dev qtbase5-dev-tools g++ python3-dev libglvnd-dev qttools5-dev libqt5svg5-dev     mpi-default-dev qtxmlpatterns5-dev-tools libqt5opengl5-dev libpugixml-dev libdouble-conversion-dev     liblz4-dev liblzma-dev libjpeg-dev libpng-dev libtiff-dev libfreetype-dev libjsoncpp-dev libeigen3-dev     python3-mpi4py libxml2-dev libhdf5-dev libnetcdf-dev nlohmann-json3-dev libsqlite3-dev libproj-dev     libtheora-dev libprotobuf-dev catch2 && update-ca-certificates &&     rm -rf /var/lib/apt/lists/*
 ---> Running in db1282b3f276
Get:1 http://security.ubuntu.com/ubuntu impish-security InRelease [110 kB]
Get:2 http://security.ubuntu.com/ubuntu impish-security/multiverse amd64 Packages [5328 B]
Get:3 http://archive.ubuntu.com/ubuntu impish InRelease [270 kB]
Get:4 http://security.ubuntu.com/ubuntu impish-security/main amd64 Packages [488 kB]
Get:5 http://security.ubuntu.com/ubuntu impish-security/universe amd64 Packages [244 kB]
Get:6 http://security.ubuntu.com/ubuntu impish-security/restricted amd64 Packages [399 kB]
Get:7 http://archive.ubuntu.com/ubuntu impish-updates InRelease [115 kB]
Get:8 http://archive.ubuntu.com/ubuntu impish-backports InRelease [101 kB]
Get:9 http://archive.ubuntu.com/ubuntu impish/restricted amd64 Packages [110 kB]
Get:10 http://archive.ubuntu.com/ubuntu impish/multiverse amd64 Packages [256 kB]
Get:11 http://archive.ubuntu.com/ubuntu impish/universe amd64 Packages [16.7 MB]
...
```

and after few dozen of minutes, an image, named `test` will be available.

It is also possible to configure a GitLab runner to run on your local machine

## Installing and configuring a local GitLab runner

The following steps have been tested on Ubuntu 22.04.

### Installing the gitlab runner executable

A detailed procedure may be found [here](https://docs.gitlab.com/runner/install/linux-repository.html).
The main commands, for an **Ubuntu** distribution are summarized hereafter.

```bash
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | sudo bash
sudo apt-get install gitlab-runner
```

Beware that the proxy has to be correctly set for the previous command to work. In case of trouble you may want to run:

```bash
sudo su
export HTTP_PROXY=http://address:port
export HTTPS_PROXY=http://address:port
curl -L "https://packages.gitlab.com/install/repositories/runner/gitlab-runner/script.deb.sh" | bash
exit
sudo apt-get install gitlab-runner
```

### Registering the runner

Once again a [detail procedure](https://docs.gitlab.com/runner/register/index.html#linux) exists. The main commands are summarized here: 

1. Create a new runner in the [GitLab dedicated page](https://gitlab.com/themys/dockerimagefactory/-/settings/ci_cd) (In this link, click on `Expand` button in front of the `Runners` section.)
   Note and keep the token given.

1. `sudo -E gitlab-runner register`

1. Enter the GitLab instance url (https://gitlab.com/)

1. Enter the token given previously in [this link](https://gitlab.com/themys/dockerimagefactory/-/settings/ci_cd).
   (In this link, click on `Expand` button in front of the `Runners` section.)

1. Enter a description for the runner. You can change this value later in the GitLab user interface.

1. Enter the tags associated with the runner, separated by commas. You can change this value later in the GitLab user interface. These tags will allow this runner to be selected by the CI process if 
the tag in the `.gitlab-ci.yml` file corresponds to one of them. You may probably want to enter someting along: `Local Machine Username` where `Username` is your user name.

1. Enter any optional maintenance note for the runner.

1. Enter docker as the runner executor.

1. Enter "ubuntu:22.04" for the default image to be used for projects.

### Configuring the runner

Edit the `/etc/gitlab-runner/config.toml` file and ensure the **Docker** runner is priviliged:

```toml
...
[[runners]]
  name = "Runner for DockerImageFactory"
  ...
  [runners.docker]
    ...
    privileged = true
    ...
```

In the same file add following lines:

```toml
...
[[runners]]
  pre_clone_script = "git config --global http.proxy http://address:port; git config --global https.proxy http://address:port"
  environment = ["https_proxy=http://address:port", "http_proxy=http://address:port", "HTTPS_PROXY=http://address:port", "HTTP_PROXY=http://address:port", "NO_PROXY=docker:2375,docker:2376"]
  ...
```

### Last steps

In order the configuration steps to be taken into account, type the following commands:

```bash
sudo systemctl daemon-reload
sudo systemctl restart docker
sudo systemctl restart gitlab-runner
```

Launch the `gitlab-runner` by typing:

```bash
sudo -E gitlab-runner start
sudo -E gitlab-runner run
```

Once done the CI process should be able to run on the local gitlab runner.

## Checking the setup and launching the process

To check the runner is valid, please type:

```bash
sudo -E gitlab-runner verify
```

If everything is ok, you should see:

```bash
Verifying runner... is valid
```

To check the runner is reachable from `gitlab.com` please go to: https://gitlab.com/themys/dockerimagefactory/-/settings/ci_cd .
If necessary, click on the `expand` button in front of the `Runners` section.
On the left column under the `Available specific runners` you should see the runner
you just created:

![](resources/available_specific_runners.png)

The green button means the runner is online and available.

To launch the process go to the [`CI/CD - Pipelines`](https://gitlab.com/themys/dockerimagefactory/-/pipelines) web page of the project and click on the `Run pipeline` button.